FIND_PATH(VCGLib_INCLUDE_DIRS vcg
        /usr/include/vcglib
        /opt/local/include/vcglib
        /usr/local/include/vcglib
        /sw/include/vcglib
        )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(VCGLib DEFAULT_MSG VCGLib_INCLUDE_DIRS)

if (VCGLib_INCLUDE_DIRS)
    set(VCGLib_FOUND TRUE)
    message(STATUS "Found VCGLib: ${VCGLib_INCLUDE_DIRS}")
else (VCGLib_INCLUDE_DIRS)
    message(VCGLib NOT found)
endif (VCGLib_INCLUDE_DIRS)
