#include <boost/tokenizer.hpp>
#include <glog/logging.h>
#include <fstream>
#include <iostream>
#include <chrono>
#include <OpenImageIO/imageio.h>

#include "grtiData.h"
#include "utils.h"


// namespace OIIO = OpenImageIO;
typedef boost::multi_array_types::index_range boost_idx_range;

GrtiData::GrtiData() {};

GrtiData::GrtiData(std::string lpPath, std::string domePath) {
  set_lp_file_path(lpPath);
  load_lp_image_paths();
  load_image_data();
  set_dome_geo_path(domePath);
  load_dome_obj();
  convert_poly_to_mesh(dome_poly, dome_mesh);
  build_lobe_array_vcg();
};

int GrtiData::set_source_image_path(std::string path) {
  source_image_path = path;
  return 0;
};

int GrtiData::set_lp_file_directory(std::string path) {
  lp_file_directory = path;
  return 0;

};

int GrtiData::set_dome_geo_path(std::string path) {
  dome_geo_path = path;
  return 0;
}

int GrtiData::set_lp_file_path(std::string path) {
  lp_file_path = path;
  set_lp_file_directory(lp_file_path.parent_path().string());
  return 0;
};

int GrtiData::load_lp_image_paths() {
// read list of paths from .lp file
  std::ifstream in(lp_file_path.string().c_str());
  if (!in.is_open()) {
    LOG(INFO) << "can not read .lp file" << std::endl;
    return 1;
  }
  typedef boost::tokenizer<boost::char_separator<char>> Tokenizer;
  boost::char_separator<char> sep(" ");
  std::vector<std::string> lp_path_vec;
  std::string line;

// first line gets read as number of images
  std::getline(in, line);
  num_images = std::stoi(line);

// following lines, with four tokens get read as lp path
  while (std::getline(in, line)) {
    Tokenizer tok(line, sep);
    lp_path_vec.assign(tok.begin(), tok.end());
//   skip line entries without 4 tokens -> "path, X, Y, Z"
    if (lp_path_vec.size() == 4) {
      lp_image_paths.push_back(lp_path_vec[0]);
    }
  }

  if ((int) lp_image_paths.size() != num_images) {
    LOG(INFO) << "number of.lp file entries does not match number of images in header." << std::endl;
    return 1;
  }

  source_image_path =
      lp_file_directory.string() + boost::filesystem::path::preferred_separator + lp_image_paths[0];
  return 0;
};

int GrtiData::load_image_data() {
  LOG(INFO) << "load images started" << std::endl;
  OIIO::ImageInput *input_image = OIIO::ImageInput::open(source_image_path.string());
  const OIIO::ImageSpec &spec = input_image->spec();

  width = spec.width;
  height = spec.height;
  channels = spec.nchannels;

  input_image->close();
  OIIO::ImageInput::destroy(input_image);

  // cycle through all images and load data into array
  source_data_arr.resize(boost::extents[num_images][height][width][channels]);
  for (int i = 0; i < num_images; ++i) {
    std::string img_path = lp_file_directory.string()
        + boost::filesystem::path::preferred_separator
        + lp_image_paths[i];

    input_image = OIIO::ImageInput::open(img_path);
    std::vector<float> pixels(height * width * channels);

    boost::multi_array<float, 4>::array_view<3>::type
        my_view = source_data_arr[boost::indices[i][boost_idx_range()][boost_idx_range()][boost_idx_range()]];
    input_image->read_image(OIIO::TypeDesc::FLOAT, my_view.origin());

    input_image->close();
    OIIO::ImageInput::destroy(input_image);
  }
  LOG(INFO) << "load images ended" << std::endl;
  return 0;
};

int GrtiData::load_dome_obj() {
  SMeshLib::IO::importOBJ(dome_geo_path.string(), &dome_poly);
//  std::printf("Dome file with %d vertices has been loaded.\n", (int) dome_poly.size_of_vertices());
  LOG(INFO) << "Dome file with " <<  (int) dome_poly.size_of_vertices() << "vertices has been loaded." << std::endl;
  return 0;
};

CgalPolyhedron GrtiData::get_dome_poly() {
  return dome_poly;
};

CgalSurfaceMesh GrtiData::get_dome_mesh() {
  return dome_mesh;
};

int GrtiData::set_dome_mesh(CgalSurfaceMesh surface_mesh) {
  dome_mesh = surface_mesh;
  return 0;
};

int GrtiData::build_lobe_array_mesh() {
  typedef boost::multi_array<float, 4> Array_4;
  typedef Array_4::index_range Range;
  typedef Array_4::array_view<1>::type Array_view;

  lobe_data_arr_mesh.resize(boost::extents[width][height][channels]);
  LOG(INFO) << "finished resizing array" << std::endl;
  LOG(INFO) << "build lobe array start" << std::endl;
  LOG(INFO) << "needs to build " << (width * height * channels) << " lobes" << std::endl;
  auto start = std::chrono::system_clock::now();

#pragma omp parallel for
  for (int y_idx = 0; y_idx < height; y_idx++) {
    for (int x_idx = 0; x_idx < width; x_idx++) {
      for (int ch_idx = 0; ch_idx < channels; ch_idx++) {
        Array_view view = source_data_arr[boost::indices[Range()][y_idx][x_idx][ch_idx]]; // TODO: these indexes confuse
        lobe_data_arr_mesh[x_idx][y_idx][ch_idx] = build_lobe_mesh<Array_view>(view);
      }
    }
  }

  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  LOG(INFO) << "finished building lobes at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n"
            << (elapsed_seconds.count() / (width * height * channels)) << "s per lobe \n"
            << (width * height * channels) << " lobes in total";

  return 0;
};

int GrtiData::build_lobe_array_vcg() {
  typedef boost::multi_array<float, 4> Array_4;
  typedef Array_4::index_range Range;
  typedef Array_4::array_view<1>::type Array_view;

  lobe_data_arr_vcg.resize(boost::extents[width][height][channels]);
  LOG(INFO) << "finished resizing array" << std::endl;
  LOG(INFO) << "build lobe array start" << std::endl;
  LOG(INFO) << "needs to build " << (width * height * channels) << " lobes" << std::endl;
  auto start = std::chrono::system_clock::now();

#pragma omp parallel for
  for (int y_idx = 0; y_idx < height; y_idx++) {
    for (int x_idx = 0; x_idx < width; x_idx++) {
      for (int ch_idx = 0; ch_idx < channels; ch_idx++) {
        Array_view view =
            source_data_arr[boost::indices[Range()][y_idx][x_idx][ch_idx]]; // TODO:: check indices
        CgalSurfaceMesh lobeMesh = build_lobe_mesh<Array_view>(view);
        auto vcgLobe = Utils::cgalMeshToVcg(lobeMesh);  // TODO: build vcg mesh instead of CGAL

//        vcg::tri::UpdateBounding<VCG::VcgMesh>::Box(*vcgLobe);
//        vcg::tri::UpdateTopology<VCG::VcgMesh>::FaceFace(*vcgLobe);
//        vcg::tri::UpdateTopology<VCG::VcgMesh>::VertexFace(*vcgLobe);
//        vcg::tri::UpdateFlags<VCG::VcgMesh>::FaceBorderFromFF(*vcgLobe);
//        vcg::tri::UpdateFlags<VCG::VcgMesh>::FaceBorderFromVF(*vcgLobe);

        lobe_data_arr_vcg[x_idx][y_idx][ch_idx] = vcgLobe;
      }
    }
  }

  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  LOG(INFO) << "finished building lobes at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n"
            << (elapsed_seconds.count() / (width * height * channels)) << "s per lobe \n"
            << (width * height * channels) << " lobes in total" << std::endl;

  return 0;
};

void GrtiData::convert_poly_to_mesh(CgalPolyhedron &inPoly, CgalSurfaceMesh &outMesh) {
  typedef CgalSurfaceMesh::Vertex_index vertex_descriptor;
  typedef CgalSurfaceMesh::Face_index face_descriptor;
  typedef CgalSurfaceMesh::Vertex_index index;

// initialize indices
  std::size_t i = 0;
  for (CgalPolyhedron::Vertex_iterator it = inPoly.vertices_begin(); it != inPoly.vertices_end(); ++it) {
    it->id() = i++;  // initialises poly vertex IDs.
    outMesh.add_vertex(Kernel::Point_3(it->point()));
  }
  i = 0;
  for (CgalPolyhedron::Facet_iterator it = inPoly.facets_begin(); it != inPoly.facets_end(); ++it) {
    it->id() = i++;  // initialises poly face IDs.
  }

//  write face vertex indices into surface_mesh
  for (CgalPolyhedron::Facet_iterator facet_iter = inPoly.facets_begin(); facet_iter != inPoly.facets_end();
       ++facet_iter
      ) {
    std::vector<size_t> vertices_vec;
    CgalPolyhedron::Halfedge_around_facet_circulator circ = facet_iter->facet_begin();

    do {
      vertices_vec.push_back(circ->vertex()->id());
    } while (++circ != facet_iter->facet_begin());

    outMesh.add_face(index(vertices_vec[0]), index(vertices_vec[1]), index(vertices_vec[2]));
    vertices_vec.clear();
  }
  if (!outMesh.is_valid()) {
    std::string _msg = "Error converting polyhedron.";
    throw std::runtime_error(_msg);
  }
};

void GrtiData::convert_poly_to_VCG(CgalPolyhedron &inPoly, VCG::VcgMesh &outVcg) {
  outVcg.Clear();
  vcg::tri::Allocator<VCG::VcgMesh>::AddVertices(outVcg, inPoly.size_of_vertices());
  vcg::tri::Allocator<VCG::VcgMesh>::AddFaces(outVcg, inPoly.size_of_facets());

  // initialize inPoly indices and add vertices to outVcg
  std::vector<VCG::VcgMesh::VertexPointer> index(outVcg.VN());
  auto vi = outVcg.vert.begin();

  size_t i = 0, j = 0;
  for (auto it = inPoly.vertices_begin(); j < inPoly.size_of_vertices(); ++it, ++j, ++vi) {
    it->id() = i++;   // initialises poly vertex IDs.
    vi->P() = VCG::VcgMesh::CoordType(it->point()[0], it->point()[1], it->point()[2]);
    index[j] = &(*vi);
  }
  // builds faces
  CgalPolyhedron::Facet_iterator fiPoly;
  CgalPolyhedron::Halfedge_around_facet_circulator circ;
  auto fiVcg = outVcg.face.begin();
  std::vector<size_t> verticesVec;

  for (i = 0, fiPoly = inPoly.facets_begin(); i < inPoly.size_of_facets(); ++fiPoly, ++fiVcg, ++i) {
    circ = fiPoly->facet_begin();
    do {
      verticesVec.push_back(circ->vertex()->id());
    } while (++circ != fiPoly->facet_begin());
    fiVcg->V(0) = index[verticesVec[0]];
    fiVcg->V(1) = index[verticesVec[1]];
    fiVcg->V(2) = index[verticesVec[2]];
    verticesVec.clear();
  }
};

int GrtiData::simplifyLobesVCG(int numVerts, bool preserveBorders) {
  LOG(INFO) << "simplify lobe array start" << std::endl;
  auto start = std::chrono::system_clock::now();

//#pragma omp parallel for // this gives a vcg Assertion error on edge collapse, update flags
  for (size_t y = 0; y < lobe_data_arr_vcg.shape()[0]; y++) {
    for (size_t x = 0; x < lobe_data_arr_vcg.shape()[1]; x++) {
      for (size_t ch = 0; ch < lobe_data_arr_vcg.shape()[2]; ch++) {
        (*lobe_data_arr_vcg[y][x][ch]).simplify(numVerts, preserveBorders);
      }
    }
  }
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  LOG(INFO) << "finished simplifing lobes at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n"
            << (elapsed_seconds.count() / (width * height * channels)) << "s per lobe \n"
            << (width * height * channels) << " lobes in total" << std::endl;
  return 0;
};

int GrtiData::subdivideLobes(int numIterations) {
  LOG(INFO) << "subdivide lobes array start" << std::endl;
  auto start = std::chrono::system_clock::now();

#pragma omp parallel for
  for (size_t i = 0; i < numIterations; i++) {
    for (size_t y = 0; y < lobe_data_arr_vcg.shape()[0]; y++) {
      for (size_t x = 0; x < lobe_data_arr_vcg.shape()[1]; x++) {
        for (size_t ch = 0; ch < lobe_data_arr_vcg.shape()[2]; ch++) {
          try {
            (*lobe_data_arr_vcg[y][x][ch]).subdivide();
          } catch (const std::exception &e) {
            continue;
          }
        }
      }
    }
  }
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  LOG(INFO) << "finished subdividing lobes at " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << "s\n"
            << (elapsed_seconds.count() / (width * height * channels)) << "s per lobe \n"
            << (width * height * channels) << " lobes in total" << std::endl;
  return 0;
};
