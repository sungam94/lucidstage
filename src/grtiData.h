#ifndef LUCIDSTAGE_GRTIDATA_H
#define LUCIDSTAGE_GRTIDATA_H

#include <boost/multi_array.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_items_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/HalfedgeDS_list.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Surface_mesh.h>

#include <fstream>

#include "ImportOBJ.h"
#include "VcgMesh.h"

/* everything named "poly" comes from CGAL::polyhedron_3, this is how I started
 * everything named "mesh" comes from CGAL::surface_mesh which is faster to move vertices in than in poly
 * everything named "vcg" comes from VCGLib and is used from mesh simplification
 * */

typedef CGAL::Simple_cartesian<double> Kernel;
typedef CGAL::Polyhedron_3<Kernel, CGAL::Polyhedron_items_with_id_3, CGAL::HalfedgeDS_list> CgalPolyhedron;
typedef Kernel::Point_3 Point_3;
typedef CGAL::Surface_mesh<Kernel::Point_3> CgalSurfaceMesh;
typedef CgalSurfaceMesh::Vertex_index Index;
typedef CGAL::Aff_transformation_3<Kernel> Aff_transformation_3;
typedef CgalPolyhedron::Vertex_iterator Vertex_iterator;
typedef CgalPolyhedron::Facet_iterator Facet_iterator;

class GrtiData {
 public:
  // This class holds all the source image data
  boost::multi_array<float, 4> source_data_arr;
  boost::multi_array<CgalPolyhedron, 3> lobe_data_arr_poly;
  boost::multi_array<CgalSurfaceMesh, 3> lobe_data_arr_mesh;
  boost::multi_array<VCG::VcgMesh *, 3> lobe_data_arr_vcg; // y, x , ch

  // source image infortmation
  int num_images;
  int width;
  int height;
  int channels;
  double *brightness;

  boost::filesystem::path source_image_path;
  std::string source_image_name;
  boost::filesystem::path lp_file_directory;

  // lp file data
  boost::filesystem::path lp_file_path;
  std::vector<std::string> lp_image_paths;

  // dome geometry data
  boost::filesystem::path dome_geo_path;

  CgalPolyhedron dome_poly;
  CgalSurfaceMesh dome_mesh;
  VCG::VcgMesh dome_vcg;

 public:
  GrtiData();
  GrtiData(std::string, std::string);

  friend class boost::serialization::access;
  // When the class Archive corresponds to an output archive, the
  // & operator is defined similar to <<.  Likewise, when the class Archive
  // is a type of input archive the & operator is defined similar to >>.
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & lobe_data_arr_vcg;
    ar & num_images;
    ar & width;
    ar & height;
    ar & channels;
  }

  int set_lp_file_directory(std::string);
  std::string get_lp_file_directory();

  int set_source_image_path(std::string);
  std::string get_source_image_path();

  int set_lp_file_path(std::string);
  std::string get_lp_file_path();

  int load_lp_image_paths();
  int load_image_data();

  int set_dome_geo_path(std::string);

  int load_dome_obj();
  CgalPolyhedron get_dome_poly();

  int set_dome_mesh(CgalSurfaceMesh);
  CgalSurfaceMesh get_dome_mesh();

  void convert_poly_to_mesh(CgalPolyhedron &, CgalSurfaceMesh &);
  void convert_poly_to_VCG(CgalPolyhedron &, VCG::VcgMesh &);

  int build_lobe_array_mesh();
  int build_lobe_array_vcg();

  int simplifyLobesVCG(int, bool);
  int subdivideLobes(int);


  template<typename View>
  CgalSurfaceMesh build_lobe_mesh(View &view) {

    float value = 0;
    CgalSurfaceMesh lobe = dome_mesh;
    for (int img = 0; img < num_images; img++) {
      value = view[img];
      if (value < 0.01) value = 0.01;
      Aff_transformation_3 scale(CGAL::SCALING, value);
      lobe.point(Index(img)) = scale(lobe.point(Index(img)));
    }
    return lobe;
  }
};

#endif //LUCIDSTAGE_GRTIDATA_H
