#include <iostream>
#include <glog/logging.h>

//#include <qapplication.h>
//#include <vtkPLYWriter.h>

#include "utils.h"

#define EPSILON 0.001

//#include "grtiViewer.h"

//void const Utils::ompTest() {
//  int num_threads = 0;
//#pragma omp parallel for
//  for (int i = 0; i < 10000; i++) {
//    double b = sqrt(i);
//    if (i == 50) num_threads = omp_get_num_threads();
//  }
//  LOG(INFO) << "num threads: " << num_threads << std::endl;
//};

// void Utils::printCgalPolyVerts(CgalPolyhedron P) {
//  typedef CgalPolyhedron::Vertex_iterator Iterator;
//  CGAL::set_ascii_mode(std::cout);
//  for (Iterator v_iterator = P.vertices_begin(); v_iterator != P.vertices_end(); ++v_iterator)
//    LOG(INFO) << v_iterator->point() << std::endl;
// };

// void Utils::printCgalMeshVerts(CgalSurfaceMesh M) {
//   CGAL::set_ascii_mode(std::cout);
//   for (auto p_iterator = M.points().begin(); p_iterator != M.points().end(); ++p_iterator)
//     LOG(INFO) << (*p_iterator) << std::endl;
// };

//int Utils::render_lobe() {
//
//  int argc;
//  char **argv;
//  QApplication application(argc, argv);
//  Viewer viewer;
//  viewer.setWindowTitle("grtiViewer");
//  viewer.show();
//  return application.exec();
//};

VCG::VcgMesh *Utils::cgalMeshToVcg(CgalSurfaceMesh &inMesh) {
  typedef CgalSurfaceMesh::Vertex_index Vertex_descriptor;
  typedef CgalSurfaceMesh::Face_index Face_descriptor;
  auto outVcg = new VCG::VcgMesh;

  vcg::tri::Allocator<VCG::VcgMesh>::AddVertices(*outVcg, inMesh.number_of_vertices());
  vcg::tri::Allocator<VCG::VcgMesh>::AddFaces(*outVcg, inMesh.number_of_faces());
  std::vector<VCG::VcgMesh::VertexPointer> index((*outVcg).VN());

  // initialize inPoly indices and add vertices to outVcg
  auto vi = (*outVcg).vert.begin();
  size_t i = 0;
  BOOST_FOREACH(Vertex_descriptor vd, inMesh.vertices()) {
          (*vi).P() = VCG::VcgMesh::CoordType(inMesh.point(vd)[0],
                                              inMesh.point(vd)[1],
                                              inMesh.point(vd)[2]);
          index[i++] = &(*vi); // index initialisation
          ++vi;
        }

  CgalPolyhedron::Facet_iterator fiPoly;
  CgalPolyhedron::Halfedge_around_facet_circulator circ;
  std::vector<Vertex_descriptor> verticesVec;
  auto fiVcg = (*outVcg).face.begin();

  BOOST_FOREACH(Face_descriptor fd, inMesh.faces()) {
          BOOST_FOREACH(Vertex_descriptor vd, CGAL::vertices_around_face(halfedge(fd, inMesh), inMesh)) {
                  verticesVec.push_back(vd);
                }
          fiVcg->V(0) = index[verticesVec[0]];
          fiVcg->V(1) = index[verticesVec[1]];
          fiVcg->V(2) = index[verticesVec[2]];
          ++fiVcg;
          verticesVec.clear();
        }
  return outVcg;
};
//
///*!
// * Converts aa vcgMesh to a vtkMesh an reduces it to a unit sphere
// * vcg mesh is needed for simplification
// * vtk mesh is needed for barycentric interpolation
// * It also returns a array with all the brightness values as convenience to multiply with weights in the interpolation function
// * @param inVcg
// * @param vtkMesh
// * @return
// */
//void Utils::vcgToVtk(VCG::VcgMesh *inVcg, vtkSmartPointer<vtkPolyData> vtkMesh, std::vector<double> *brightnessPtr) {
//
//  vtkSmartPointer<vtkPoints> myVtkPoints = vtkSmartPointer<vtkPoints>::New();
//  vtkSmartPointer<vtkCellArray> myVtkPolys = vtkSmartPointer<vtkCellArray>::New();
//
//  vtkIdType pointIds[inVcg->VN()];
//  int idx = 0;
//  for (auto &&item : pointIds) {
//    item = idx++;
//  }
//
//  float pointNormalized[3];
//  float length;
//  for (auto vi = inVcg->vert.begin(); vi != inVcg->vert.end(); ++vi) {
//    auto p = vi->P();
//    length = sqrt(p[0] * p[0] + p[1] * p[1] + p[2] * p[2]);
//    brightnessPtr->push_back(length);
//    if (length != 0) {
//      pointNormalized[0] = 1 / length * p[0];
//      pointNormalized[1] = 1 / length * p[1];
//      pointNormalized[2] = 1 / length * p[2];
//      myVtkPoints->InsertNextPoint(pointNormalized[0], pointNormalized[1], pointNormalized[2]);
//    } else {
//      myVtkPoints->InsertNextPoint(0, 0, 0);
//    }
//  }
//
//  // TODO: replace vtkCellArray with IdList for tris
//  vtkIdType face[3];
//  for (auto fi = inVcg->face.begin(); fi != inVcg->face.end(); ++fi) {
//    face[0] = Utils::getVcgVertId(inVcg, fi->V(0));
//    face[1] = Utils::getVcgVertId(inVcg, fi->V(1));
//    face[2] = Utils::getVcgVertId(inVcg, fi->V(2));
//    myVtkPolys->InsertNextCell(3, face);
//  }
//
//  vtkMesh->SetPoints(myVtkPoints);
//  vtkMesh->SetPolys(myVtkPolys);
//}

std::vector<double> Utils::cartToSphere(std::vector<double> inCoords) {
  std::vector<double> outCoords;
  double r = std::sqrt(inCoords[0] * inCoords[0] + inCoords[1] * inCoords[1] + inCoords[2] * inCoords[2]);
  double phi = std::atan2(inCoords[1], inCoords[0]);
  double theta = std::acos(inCoords[2] / r);

  outCoords.push_back(r);
  outCoords.push_back(phi);
  outCoords.push_back(theta);
  return outCoords;
};

/**
 * Convert spherical coordinates to three dimensional carthesian coordinates.
 * @param inCoords radius r,
 *                 azimuth phi,
 *                 inclination 90 - theta -> northpole to horizon
 * @return X,
 *         Y,
 *         Z is up
 */
std::vector<double> Utils::sphereToCart(std::vector<double> inCoords) {
  std::vector<double> outCoords;
  double X = inCoords[0] * sin(inCoords[2]) * cos(inCoords[1]);
  double Y = inCoords[0] * sin(inCoords[2]) * sin(inCoords[1]);
  double Z = inCoords[0] * cos(inCoords[2]);
  outCoords.push_back(X);
  outCoords.push_back(Y);
  outCoords.push_back(Z);
  return outCoords;
};

vcg::Point3f Utils::sphereToCartVcg(std::vector<double> inCoords) {
  vcg::Point3f outCoords;
  outCoords[0] = inCoords[0] * sin(inCoords[2]) * cos(inCoords[1]);
  outCoords[1] = inCoords[0] * sin(inCoords[2]) * sin(inCoords[1]);
  outCoords[2] = inCoords[0] * cos(inCoords[2]);
  return outCoords;
};



/*!
 * Converts latitude and logitude
 * @param latLong x, y normalized
 * @return r, -> 1.0 unit sphere
 *         phi, azimuth in rad
 *         theta, 0 - theat -> is northpole to horizon in rad
 */
std::vector<double> Utils::latLongToSpherical(std::vector<double> latLong) {
  std::vector<double> outCoords;
  double phi = 2 * M_PI * latLong[2];
  double theta = M_PI / 2 * latLong[1];
  outCoords.push_back(1.0);
  outCoords.push_back(phi);
  outCoords.push_back(theta);
  return outCoords;
}

/*!
 * @param latLong r, phi, theta (90-theta -> northpole to horizon)
 * @return x, y as factor
 */
std::vector<double> sphericalToLatLong(std::vector<double> latLong) {
  std::vector<double> outCoords;
  double r = latLong[0];
  double phi = latLong[1];
  double phi_a = 0;
  double phi_b = 0;
  double phi_c = 0;
  double theta = latLong[2];
  phi_a = phi + M_PI; // add PI to get (0:2PI) instead of (-PI:PI) range
  (phi_a > 0) ? phi_b = ((2 * M_PI) / phi_a) : phi_b = 0; // map to 2PI
  (phi_b > 0) ? phi_c = (1 / phi_b) : phi_c = 0;
  outCoords.push_back(phi_c);
  outCoords.push_back(((1 / (M_PI / 4 / theta)) * 1) / 2);
  return outCoords;
}



//public static LatLon FromVector3(Vector3 position, float sphereRadius)
//{
//  float lat = (float)Math.Acos(position.Y / sphereRadius); //theta
//  float lon = (float)Math.Atan(position.X / position.Z); //phi
//  return new LatLon(lat, lon);
//}

//void Utils::saveVtkAsPly(std::string filename, vtkSmartPointer<vtkPolyData> myVtkMesh) {
//  vtkSmartPointer<vtkPLYWriter> plyWriter =
//      vtkSmartPointer<vtkPLYWriter>::New();
//  plyWriter->SetFileName(filename.c_str());
//  plyWriter->SetInputData(myVtkMesh);
//  plyWriter->Write();
//};

int Utils::getVcgVertId(VCG::VcgMesh *vcgMesh, VCG::VcgMesh::VertexPointer vertPtr) {
  /**
   * There should be a better way
   * VCG does not provide vertex IDs, as long as the mesh does not change, one should be able to pick it
   */
  int idx = 0;
  auto vert = &(*vertPtr);
  for (auto vi = vcgMesh->vert.begin(); vi != vcgMesh->vert.end(); ++vi, ++idx) {
    auto viVert = &(*vi);
    if (viVert == vert) return idx;
  }
  return -1;
};


//std::vector<double> Utils::sph_to_cyl_coord(std::vector<double> inCoords) {
//  std::vector<double> outCoords;
//  double r = std::sqrt(inCoords[0] * inCoords[0] + inCoords[1] * inCoords[1] + inCoords[2] * inCoords[2]);
//  double phi = std::atan2(inCoords[1], inCoords[0]);
//  double theta = std::acos(inCoords[2] / r);
//
//  outCoords.push_back(r);
//  outCoords.push_back(phi);
//  outCoords.push_back(theta);
//  return outCoords;
//};



std::vector<double> Utils::rad_to_deg(std::vector<double> inRad) {
  std::vector<double> outDeg;
  for (auto rad : inRad) {
    outDeg.push_back(rad * (180 / M_PI));
  }
  return outDeg;
};

//template <class PolyhedronTraits_3>
//int Utils::open_in_geom(PolyhedronTraits_3 poly) {
//  Geomview_stream& operator<<( Geomview_stream& out,
//                               const CGAL::Polyhedron_3<PolyhedronTraits_3>& P);
//};


//bool Utils::pointsClose(const vcg::Point3f pa,
//                 const vcg::Point3f pb) {
//  static const float pointsCloseDistance = 1e-5;
//  return ((fabs(pa[0] - pb[0]) < pointsCloseDistance) &&
//      (fabs(pa[1] - pb[1]) < pointsCloseDistance) &&
//      (fabs(pa[2] - pb[2]) < pointsCloseDistance));
//}


//double Utils::solidAngle(const vcg::Point3f &p0, const vcg::Point3f &p1, const vcg::Point3f &p2) {
//
//  // idea by Torsten
//
//  // the covered solid angle equals the spherical excess, which can be expressed
//  // efficienlty using the inner angles of the triangle's corners. These are the
//  // intersection angles of the planes which define the triangle
//
//  // thus: compute the angles
//
//  // in case of degenerate triangles, create a zero angle instead of NaN
//
//  // implementaion by Dr. Martin Fuchs
//
//  if (pointsClose(p0, p1) || pointsClose(p0,p2) || pointsClose(p1,p2) ) {
//    return 0.;
//  }
//
//  // caveat: the plane normals
//  // need to rotate around the same axis to be correct (thus: not p0%p1 | p1%p0 !)
//
//  double alpha  = acos (std::max(-1., std::min(1.0f, (p0 ^ p1).normalized() * (p0 ^ p2).normalized() )));
//  double beta   = acos (std::max(-1., std::min(1.0f, (p1 ^ p2).normalized() * (p1 ^ p0).normalized() )));
//  double gamma  = acos (std::max(-1., std::min(1.0f, (p2 ^ p0).normalized() * (p2 ^ p1).normalized() )));
//
//  return (alpha + beta + gamma) - M_PI;
//}
//


vcg::Point3f *Utils::calcBarycentrics(vcg::Point3f *triangle, vcg::Point3f P) {
  vcg::Point3f unitTriangle[3];

  for (int i = 0; i < 3; i++) {
    float length = vcg::Norm(triangle[i]);
    if (length > FLT_EPSILON) {

      float pX = 1 / length * triangle[i][0];
      float pY = 1 / length * triangle[i][1];
      float pZ = 1 / length * triangle[i][2];
      unitTriangle[i] = VCG::VcgMesh::CoordType(pX, pY, pZ);
    } else {
      unitTriangle[i] =
          VCG::VcgMesh::CoordType(0, 0, 0); // length == 0, should already be prevented in simplifying step
    }
  };

  float solidAngleOnSphere = solidAngle(unitTriangle[0], unitTriangle[1], unitTriangle[2]);
  float w0 = solidAngle(P, unitTriangle[1], unitTriangle[2]) / solidAngleOnSphere;
  float w1 = solidAngle(unitTriangle[2], unitTriangle[0], P) / solidAngleOnSphere;
  float w2 = solidAngle(unitTriangle[1], P, unitTriangle[0]) / solidAngleOnSphere;

  auto *weightsPtr = new vcg::Point3f(w0, w1, w2);

  if ((w0 > EPSILON) && (w1 > EPSILON) && (w2 > EPSILON)) { //
//    LOG(INFO)<< "weights: " << w0 << ", " << w1 << ", " << w2 << std::endl;
    return weightsPtr;
  } else {
    return nullptr;
  }


}

bool Utils::pointsClose(const vcg::Point3f pa, const vcg::Point3f pb) {
  static const float pointsCloseDistance = 1e-5;
  return ((fabs(pa[0] - pb[0]) < pointsCloseDistance) &&
      (fabs(pa[1] - pb[1]) < pointsCloseDistance) &&
      (fabs(pa[2] - pb[2]) < pointsCloseDistance));
};

float Utils::solidAngle(const vcg::Point3f &p0, const vcg::Point3f &p1, const vcg::Point3f &p2) {
  // in case of degenerate triangles, create a zero angle instead of NaN
  if (pointsClose(p0, p1) || pointsClose(p0, p2) || pointsClose(p1, p2)) {
    return 0.0f;
  }

  float alpha = acos(std::max(-1.0f, std::min(1.0f, (p0 ^ p1).normalized() * (p0 ^ p2).normalized())));
  float beta = acos(std::max(-1.0f, std::min(1.0f, (p1 ^ p2).normalized() * (p1 ^ p0).normalized())));
  float gamma = acos(std::max(-1.0f, std::min(1.0f, (p2 ^ p0).normalized() * (p2 ^ p1).normalized())));

  return (alpha + beta + gamma) - (float) M_PI;
};

VCG::VcgMesh *Utils::makeUnitSphere(VCG::VcgMesh *vcgMesh) {
  for (auto vi = vcgMesh->vert.begin(); vi != vcgMesh->vert.end(); ++vi) {
    float length = vcg::Norm(vi->P());
    if (length != 0) {
      float pX = 1 / length * vi->P()[0];
      float pY = 1 / length * vi->P()[1];
      float pZ = 1 / length * vi->P()[2];
      vi->P() = VCG::VcgMesh::CoordType(pX, pY, pZ);
    } else {
      vi->P() = VCG::VcgMesh::CoordType(0, 0, 0); // length == 0, should already be prevented in simplifying step
    }
  };
};
