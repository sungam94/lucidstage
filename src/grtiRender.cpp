#include <glog/logging.h>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <chrono>
//#include <vtkMeanValueCoordinatesInterpolator.h>
#include <vcg/space/point3.h>
#include <OpenImageIO/imageio.h>

#include "grtiRender.h"

#define EPSILON 0.001

/*
 * Regular functions have mixed case; accessors and mutators match the name of the variable:
 * MyExcitingFunction(), MyExcitingMethod(),
 * my_exciting_member_variable(), set_my_exciting_member_variable().
 */

GrtiRender::GrtiRender(std::size_t w, std::size_t h, GrtiData *grtiDataPtr) {
  m_width = w;
  m_height = h;

  m_Geometry.width(w);
  m_Geometry.height(h);
  m_Image.size(m_Geometry);
  m_Image.backgroundColor("black");
  m_GrtiDataPtr = grtiDataPtr;

  m_image_OIIO.resize(boost::extents[m_height][m_width][3]);


};

void GrtiRender::drawPoint(double phi, double theta, std::vector<double> color, int size) {
  double phi_a = 0;
  double phi_b = 0;
  double phi_c = 0;

  phi_a = phi + M_PI; // add PI to get (0:2PI) instead of (-PI:PI) range
  (phi_a > 0) ? phi_b = ((2 * M_PI) / phi_a) : phi_b = 0; // map to 2PI
  (phi_b > 0) ? phi_c = (1 / phi_b) : phi_c = 0;
  auto x = (size_t) (phi_c * m_width);
  auto y = (size_t) ((1 / (M_PI / 4 / theta)) * m_height) / 2;

  // Set draw options
  m_Image.strokeColor(Magick::ColorRGB(color[0], color[1], color[2])); // Outline color
  m_Image.fillColor(Magick::ColorRGB(color[0], color[1], color[2])); // Fill color
  m_Image.strokeWidth(0); // Draw a circle
  m_Image.draw(Magick::DrawableCircle(x, y, x, y + size));
};

void GrtiRender::saveImage(std::string fp, boost::multi_array<float, 3> image) {
  float *pixels = image.data();

  const char *filename = fp.c_str();
  int channels = 3;
  OIIO::ImageOutput *out = OIIO::ImageOutput::create(filename);
  if (!out)
    return;
  OIIO::ImageSpec spec(image.shape()[0], image.shape()[2], channels, OIIO::TypeDesc::FLOAT);
  out->open(filename, spec);
  out->write_image(OIIO::TypeDesc::FLOAT, pixels);
  out->close();
};

void GrtiRender::saveEnvMap(std::string directory, std::string baseName) {
  boost::filesystem::path path = directory;
  std::string
      name = boost::str(boost::format{"%s_%s_%s.exr"} % baseName % std::to_string(m_xData) % std::to_string(m_yData));
  path.append(name);
  float *pixels = m_image_OIIO.data();
  const char *filename = path.c_str();
  int channels = 3;
  OIIO::ImageOutput *out = OIIO::ImageOutput::create(filename);
  if (!out)
    return;
  OIIO::ImageSpec spec(m_width, m_height, channels, OIIO::TypeDesc::FLOAT);
  out->open(filename, spec);
  out->write_image(OIIO::TypeDesc::FLOAT, pixels);
  out->close();
};

void GrtiRender::renderDots(const int x, const int y) {
  // myData->simplifyLobesVCG(50, true);
  std::vector<double> pointR;
  std::vector<double> pointG;
  std::vector<double> pointB;

  auto numR = m_GrtiDataPtr->lobe_data_arr_vcg[y][x][0]->VN();
  auto numG = m_GrtiDataPtr->lobe_data_arr_vcg[y][x][1]->VN();
  auto numB = m_GrtiDataPtr->lobe_data_arr_vcg[y][x][2]->VN();
  assert(numR == numG || numR == numB || numG == numB); // not sure if this makes even sense to check?

  for (int i = 0; i < numR; i++) {
    auto pR = m_GrtiDataPtr->lobe_data_arr_vcg[y][x][0]->vert[i].P();
    auto pG = m_GrtiDataPtr->lobe_data_arr_vcg[y][x][1]->vert[i].P();
    auto pB = m_GrtiDataPtr->lobe_data_arr_vcg[y][x][2]->vert[i].P();

    // build vector from points at i
    pointR.clear();
    pointG.clear();
    pointB.clear();
    pointR.push_back(pR[0]);
    pointR.push_back(pR[1]);
    pointR.push_back(pR[2]);
    pointG.push_back(pG[0]);
    pointG.push_back(pG[1]);
    pointG.push_back(pG[2]);
    pointB.push_back(pB[0]);
    pointB.push_back(pB[1]);
    pointB.push_back(pB[2]);
//    Utils::display(pointPos);
    auto point_spherical = Utils::cartToSphere(pointR);
    double R = Utils::getMagnitude(pointR);
    double G = Utils::getMagnitude(pointG);
    double B = Utils::getMagnitude(pointB);

    std::vector<double> color = {R, G, B};
    drawPoint(point_spherical[1], point_spherical[2], color, 4);
  }
  char name[50];
  sprintf(name, "../output/%d_%d_dots.png", x, y);
  m_Image.write(name);

//  char const *fp1 = "80_20_1_reduced.obj";
//  VCG::VcgMesh *vcgLobe1 = myData->lobe_data_arr_vcg[80][20][1];
//  Utils::save_vcg<VCG::VcgMesh>(*vcgLobe1, fp1);
};

/* This function renders a environment map for the pixel x and pixel y of the GRTI data set.
 * Every pixel of the output map is transformed into a coordinate on the unit sphere resembling the dome.
 *          already done by calcWeights: The lobe geometry is normalized to a unit sphere radius (cause of that a lobe must never have a point at the origin, otherwise it can not be scaled back to the surface of a unit sphere)
 * Now we get the barycentric coordinates for the pixel coordinate in the space of the lobe unit sphere.
 * This should return three weights.
 * These three weights are used to mix the pixel color from the intensity of the original lobe.
 * The weight list gives the vertex index and the weight a multiplicator for the color at index.
 * If this is repeated for all pixels in the map we should have the environment map for the pixel x, y in the GRTI data set.
*/
void GrtiRender::renderEnvMap(const int xData,
                              const int yData,
                              const boost::filesystem::path outputPath,
                              const bool saveLobes) {
  LOG(INFO) << "render start" << std::endl;
  auto start = std::chrono::system_clock::now();

  m_xData = xData;
  m_yData = yData;

  double phi;
  double theta;

  double chIntensity;

  // loop through the env map and set pixels
//#pragma omp parallel for
  for (int ch = 0; ch < 3; ++ch) {
    // converts vcg mesh to vtk mesh and gets intensity values of all mesh points
    VCG::VcgMesh *lobe = m_GrtiDataPtr->lobe_data_arr_vcg[m_xData][m_yData][ch];

    if (saveLobes) {
      boost::filesystem::path path = outputPath;
      std::string name = boost::str(
          boost::format{"lobe_%s_%s_%s.obj"} % std::to_string(m_xData) % std::to_string(m_yData) % std::to_string(ch));
      path.append(name);
      Utils::saveVcg(*lobe, path.c_str());
    }

    for (int yEnv = 0; yEnv < m_height; ++yEnv) {
      for (int xEnv = 0; xEnv < m_width; ++xEnv) {
        // calculate spherical angles from pixel coordinate in env map
        phi = (2 * M_PI) * ((double) xEnv / ((double) m_width));
        theta = (M_PI / 2) * ((double) yEnv / ((double) m_height));

        // calculates barycentric weights
        chIntensity = getIntensity(lobe, phi, theta, ch);
        m_image_OIIO[yEnv][xEnv][ch] = chIntensity;

      }
    }
  }
  this->saveEnvMap(outputPath.c_str(), "reflectanceMap");

  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  std::time_t end_time = std::chrono::system_clock::to_time_t(end);
  LOG(INFO) << "finished rendering " << std::ctime(&end_time)
            << "elapsed time: " << elapsed_seconds.count() << std::endl;


};

void GrtiRender::renderAllMaps(const boost::filesystem::path outputPath,
                               const bool saveLobes) {
  const long width = this->m_GrtiDataPtr->lobe_data_arr_vcg.shape()[0];
  const long height = this->m_GrtiDataPtr->lobe_data_arr_vcg.shape()[1];

  #pragma omp parallel for
  for (int x = 0; x < width; x++) {
    for (int y = 0; y < height; y++) {
      LOG(INFO) << "rendering map: " << x << ", " << y << std::endl;
      renderEnvMap(x, y, outputPath, saveLobes);
    }
  }
};


float GrtiRender::getIntensity(VCG::VcgMesh *vcgMesh,
                               double phi,
                               double theta,
                               int channel) {



// iterate over vcgLobe and scale to unit sphere
//  for (auto vi = vcgUnitSphere.vert.begin(); vi != vcgUnitSphere.vert.end(); ++vi) {
//    float length = vcg::Norm(vi->P());
//    if (length != 0) {
//      float pX = 1 / length * vi->P()[0];
//      float pY = 1 / length * vi->P()[1];
//      float pZ = 1 / length * vi->P()[2];
//      vi->P() = VCG::VcgMesh::CoordType(pX, pY, pZ);
//    } else {
//      vi->P() = VCG::VcgMesh::CoordType(0, 0, 0); // length == 0, should already be prevented in simplifying step
//    }
//  };
// - should replace the stuff above
//  Utils::makeUnitSphere(&vcgUnitSphere);

// - disabled save of lobe
//  boost::filesystem::path path = "../output/unitSphere/";
//  std::string name = boost::str(
//      boost::format{"unitSphere_%s_%s_%s.obj"} % std::to_string(phi) % std::to_string(theta) % std::to_string(channel));
//  path.append(name);
//  Utils::saveVcg(vcgUnitSphere, path.c_str());

  // converts the point to check for to carthesian coordiantes
  vcg::Point3f d = Utils::sphereToCartVcg({1.0, phi, theta}); // azimuth phi, inclination theta
//  vcg::Point3f orig = vcg::Point3f(0.0f, 0.0f, 0.0f);

  vcg::Point3f *weights;
  vcg::Point3f bestWeights = VCG::VcgMesh::CoordType(0, 0, 0);
  float bestSum;
  float sum;
  vcg::Point3f bestTri[3];
  bool firstPass = true;

  // fiLobe iterates over the simplified lobe, where we read the intensities from
  for (auto fiLobe = vcgMesh->face.begin(); fiLobe != vcgMesh->face.end(); ++fiLobe) {
    // extract triangle points
    vcg::Point3f triangle[3];
    triangle[0] = fiLobe->P(0);
    triangle[1] = fiLobe->P(1);
    triangle[2] = fiLobe->P(2);


    // insert line edge cut from PDF from Bernd
    // loop over all faces, find intersection, then project and go to Utils::calcBarycentrics
//    vcg::Point3f e1 = triangle[1] - triangle[0];
//    vcg::Point3f e2 = triangle[2] - triangle[0];
//    vcg::Point3f p = d ^ e2;
//    float a = e1 * p;
//    if( (a > (-1 * EPSILON)) && (a < EPSILON) ) {
//      fiLobe++;
//      continue;
//    }
//    float f =  1 / a;
//    vcg::Point3f s = orig - triangle[0];
//    float u = f * (s * p);
//    if( (u < 0) || (u > 1) || (u != u)){
//      fiLobe++;
//      continue;
//    }
//    vcg::Point3f q = s ^ e1;
//    float v = f * (d * q);
//    if(v < 0 || (u + v) > 1) {
//      fiLobe++;
//      continue;
//    }
//    float t = f * (e2 * q);
//    if( t > 0.0000001){
//      fiLobe++;
//      continue;
//    }

    // and get the barycentric weights
    weights = Utils::calcBarycentrics(triangle, d);
    if (weights) {
      sum = abs(((*weights)[0] + (*weights)[1] + (*weights)[2]) - 1);
      if (firstPass) {
        firstPass = false;
        bestSum = sum;
        bestWeights[0] = (*weights)[0];
        bestWeights[1] = (*weights)[1];
        bestWeights[2] = (*weights)[2];
        bestTri[0] = triangle[0];
        bestTri[1] = triangle[1];
        bestTri[2] = triangle[2];
      } else if (sum < bestSum) {
        bestSum = sum;
//        LOG(INFO) << "bestSum: " << bestSum << std::endl;
        bestWeights[0] = (*weights)[0];
        bestWeights[1] = (*weights)[1];
        bestWeights[2] = (*weights)[2];
        bestTri[0] = triangle[0];
        bestTri[1] = triangle[1];
        bestTri[2] = triangle[2];
      }
    }
    delete weights;
  }

  // multiply them by the intesity of the input vcgMesh lobe
  float p0 = bestTri[0].Norm();
  float p1 = bestTri[1].Norm();
  float p2 = bestTri[2].Norm();
//  LOG(INFO) << "bestSum: " << bestSum << std::endl;
//  LOG(INFO) << "w0: " << bestWeights[0] << " w0: " << bestWeights[1] << " w0: " << bestWeights[2] << std::endl;
//  LOG(INFO) << "p0: " << p0 << " p1: " << p1 << " p2: " << p2 << std::endl;

  float intensity = (bestWeights[0] * p0 + bestWeights[1] * p1 + bestWeights[2] * p2);

//    LOG(INFO) << "weights: " << (*weights)[0] << ", " << (*weights)[1] << ", " << (*weights)[2] << std::endl;

  return intensity; // if weights are returned, no need to iterate further

};
