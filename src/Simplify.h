#ifndef LUCIDSTAGE_SIMPLIFY_H
#define LUCIDSTAGE_SIMPLIFY_H

#include <vcg/complex/algorithms/local_optimization.h>
#include <vcg/complex/algorithms/local_optimization/tri_edge_collapse_quadric.h>
#include <vcg/complex/complex.h>
#include "VcgMesh.h"

typedef vcg::tri::BasicVertexPair<VCG::VcgVertex> VertexPair;

class MyTriEdgeCollapse : public vcg::tri::TriEdgeCollapseQuadric<VCG::VcgMesh,
                                                                  VertexPair,
                                                                  MyTriEdgeCollapse,
                                                                  vcg::tri::QInfoStandard<VCG::VcgVertex> > {
 public:
  typedef vcg::tri::TriEdgeCollapseQuadric<VCG::VcgMesh,
                                           VertexPair,
                                           MyTriEdgeCollapse,
                                           vcg::tri::QInfoStandard<VCG::VcgVertex>> TECQ;
  typedef VCG::VcgMesh::VertexType::EdgeType EdgeType;
  inline MyTriEdgeCollapse(const VertexPair &p, int i, vcg::BaseParameterClass *pp) : TECQ(p, i, pp) {}
};

#endif //LUCIDSTAGE_SIMPLIFY_H
