#ifndef LUCIDSTAGE_UTILS_H
#define LUCIDSTAGE_UTILS_H

#include <iostream>
#include <fstream>

#include <boost/multi_array.hpp>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/IO/Polyhedron_geomview_ostream.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Surface_mesh_simplification/edge_collapse.h> // Simplification function
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Count_stop_predicate.h> // Stop-condition policy
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Edge_length_cost.h>
#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Midpoint_placement.h>

//#include <vtkPolyData.h>

#include "VcgMesh.h"

#include <vcg/complex/algorithms/local_optimization/tri_edge_collapse_quadric.h>
#include <wrap/io_trimesh/export.h>


typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_3 Point_3;
typedef CGAL::Polyhedron_3<Kernel, CGAL::Polyhedron_items_with_id_3, CGAL::HalfedgeDS_list> CgalPolyhedron;
typedef CGAL::Surface_mesh<Point_3> CgalSurfaceMesh;

namespace Utils {
template<typename Container>
std::ostream &display(const Container &container, const char *s = " ", std::ostream &os = std::cout) {
  for (const auto &value : container) {
    os << value << s;
  }
  std::cout << std::endl;
  return os;
}

template<typename Container>
void printMagnitude(const Container &container) {
  double sum = 0;
  int count = 0;
  for (const auto &value : container) {
    sum += value * value;
    count++;
  }
  if (count > 0) {
    std::cout << "magnitude: " << sqrt(sum) << std::endl;
  } else {
    std::cout << "zero division in magnitude" << std::endl;
  }
}


template<typename MeshType>

int saveVcg(MeshType &mesh, const char *fp) {
  vcg::tri::io::ExporterOBJ<MeshType>::Save(mesh, fp, 0);
  return 0;
}

template<typename T>
double getMagnitude(std::vector<T> vec) {
  double sum = 0;
  for (auto &&item : vec) {
    sum += item * item;
  }
  sum = std::sqrt(sum);
  return sum;
}

template<typename T>
int normalizePoint(T *point) {
  float length = sqrt(point[0] * point[0] + point[1] * point[1] + point[2] * point[2]);
  point[0] = 1 / length * point[0];
  point[1] = 1 / length * point[1];
  point[1] = 1 / length * point[1];
  return 0;
};

template<typename T>
double deg2rad(T deg) {
  return 2 * M_PI / 360 * deg;
};

template<typename T>
int sgn(T val) {
  return (T(0) < val) - (val < T(0));
};

void const ompTest();

void printCgalPolyVerts(CgalPolyhedron);

void printCgalMeshVerts(CgalSurfaceMesh);

VCG::VcgMesh *cgalMeshToVcg(CgalSurfaceMesh &inMesh);

///*!
// * Converts a VCG mesh to a vtk mesh of type vtkPolyData.
// * @param inVcg
// * @param vtkMesh
// * @param brightnessPtr vector to store brightness values in
// * @return
// */
//void vcgToVtk(VCG::VcgMesh *inVcg, vtkSmartPointer<vtkPolyData> vtkMesh, std::vector<double> *brightnessPtr);

/**
 * Converts three dimensional carthesian coordinates into spherical coordinates.
 * @param inCoords X,
 *                 Y,
 *                 Z is up
 * @return radius      r,
 *         azimuth     phi,
 *         inclination 90 - theta -> northpole to horizon
 */
std::vector<double> cartToSphere(std::vector<double> inCoords);

/**
 * Convert spherical coordinates to three dimensional carthesian coordinates.
 * @param inCoords radius r,
 *                 azimuth phi,
 *                 inclination 90 - theta -> northpole to horizon
 * @return X,
 *         Y,
 *         Z is up
 */
std::vector<double> sphereToCart(std::vector<double> inCoords);
vcg::Point3f sphereToCartVcg(std::vector<double> inCoords);

std::vector<double> rad_to_deg(std::vector<double>);

std::vector<double> latLongToSpherical(std::vector<double>);

//void saveVtkAsPly(std::string filename, vtkSmartPointer<vtkPolyData> myVtkMesh);

/**
 * Returns an ID for the vertex.
 * ID is the position in the vertex list.
 * This returns unpredictable results if mesh has deleted vertices or has been modified after initialization.
 * The VertexPointer gets compared to any Vertex in the mesh. If the adress is the same, an index counter is returned.
 * @param vcgMesh VcgMesh to read ID from.
 * @param vertPtr VertexPointer to vertex to find ID of.
 * @return returns vertex id, and -1 if something went wrong
 */
int getVcgVertId(VCG::VcgMesh *vcgMesh, VCG::VcgMesh::VertexPointer vertPtr);

bool pointsClose(const vcg::Point3f pa, const vcg::Point3f pb);

float solidAngle(const vcg::Point3f &p0, const vcg::Point3f &p1, const vcg::Point3f &p2);

vcg::Point3f *calcBarycentrics(vcg::Point3f *triangle, vcg::Point3f P);

VCG::VcgMesh *makeUnitSphere(VCG::VcgMesh *vcgMesh);

//int open_in_geom(PolyhedronTraits_3);
//int render_lobe();

}; // namespace Utils
#endif //LUCIDSTAGE_UTILS_H
