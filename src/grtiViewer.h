#ifndef LUCIDSTAGE_GRTIVIEWER_H
#define LUCIDSTAGE_GRTIVIEWER_H

#include <QGLViewer/qglviewer.h>

class Viewer : public QGLViewer {
 protected:
  virtual void draw();
  virtual void init();
  virtual QString helpString() const;
};

#endif //LUCIDSTAGE_GRTIVIEWER_H
