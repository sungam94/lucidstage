// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <fstream>
#include <glog/logging.h>
#include <iostream>
#include <vcg/complex/algorithms/refine_loop.h>

#include "grtiData.h"
#include "grtiRender.h"
//#include "grtiViewer.h"


std::string loewenkopf_lp = "../../LightDome_Aufnahmen/loewenkopf_cropped/loewenkopf_cropped.lp";
std::string oelfarben_lp  = "../../LightDome_Aufnahmen/oelfarben_cropped/oelfarben_cropped.lp";
std::string render_lp     = "../../LightDome_Aufnahmen/render/render_01.lp";
std::string render_37_lp  = "../../LightDome_Aufnahmen/render_37/render_37.lp";

std::string dome           = "/Users/mag/Documents/LucidStage/LucidStage/meshes/dome_240_Zup.obj";
std::string dome_render    = "/Users/mag/Documents/LucidStage/LucidStage/meshes/dome_render.obj";
std::string dome_render_37 = "/Users/mag/Documents/LucidStage/LucidStage/meshes/dome_render_37.obj";



int main(int argc, char *argv[]) {
  google::InitGoogleLogging(argv[0]);
//  FLAGS_logtostderr = 1;
  FLAGS_stderrthreshold = 0;
  FLAGS_minloglevel = 0;
  // SETUP directory names
  boost::filesystem::path outputPath = "../output/";
  std::string folderName = "loewenkopf_150_noSubdiv/";
  outputPath.append(folderName);
  boost::filesystem::create_directory(outputPath);


  // Simplify
  GrtiData *myDataPtr = new GrtiData(loewenkopf_lp, dome);
  myDataPtr->simplifyLobesVCG(150, true);
  // Subdivide
//  myDataPtr->subdivideLobes(1);


  // render reflectance function maps
  GrtiRender *myRenderPtr = new GrtiRender(512, 256, myDataPtr);
//  myRenderPtr->renderEnvMap(16, 16, outputPath);
  myRenderPtr->renderAllMaps(outputPath, false);
  return 0;
};
