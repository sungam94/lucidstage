#include <vcg/complex/algorithms/refine_loop.h>
#include <vcg/complex/complex.h>
#include "VcgMesh.h"

void VCG::VcgMesh::subdivide() {
  vcg::tri::UpdateTopology<VCG::VcgMesh>::FaceFace(*this);
  vcg::tri::RefineOddEven<VCG::VcgMesh>(*this,
                                        vcg::tri::OddPointLoop<VCG::VcgMesh>(*this),
                                        vcg::tri::EvenPointLoop<VCG::VcgMesh>(),
                                        0);
};
