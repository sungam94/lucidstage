#ifndef LUCIDSTAGE_VCGMESH_H
#define LUCIDSTAGE_VCGMESH_H

#include <vcg/complex/complex.h>
#include <vcg/math/quadric.h>

namespace VCG {

class VcgVertex;
class VcgEdge;
class VcgFace;

struct VcgUsedTypes
    : public vcg::UsedTypes<vcg::Use<VcgVertex>::AsVertexType,
                            vcg::Use<VcgEdge>::AsEdgeType,
                            vcg::Use<VcgFace>::AsFaceType> {
};

class VcgVertex
    : public vcg::Vertex<VcgUsedTypes,
                         vcg::vertex::VFAdj,
                         vcg::vertex::Coord3f,
                         vcg::vertex::Normal3f,
                         vcg::vertex::Mark,
                         vcg::vertex::BitFlags> {
 public:
  vcg::math::Quadric<double> &Qd() { return q; }
 private:
  vcg::math::Quadric<double> q;
};

class VcgFace
    : public vcg::Face<VcgUsedTypes,
                       vcg::face::FFAdj,
                       vcg::face::VFAdj,
                       vcg::face::VertexRef,
                       vcg::face::BitFlags> {
};
class VcgEdge
    : public vcg::Edge<VcgUsedTypes> {
};

class VcgMesh : public vcg::tri::TriMesh<std::vector<VcgVertex>,
        // std::vector<VcgEdge>,
                               std::vector<VcgFace> > {
 public:
  void simplify(int target_faces, bool preserve_boundary);
  void subdivide();
};
} // namespace vcg ends

#endif //LUCIDSTAGE_VCGMESH_H
