#include "Simplify.h"

void VCG::VcgMesh::simplify(int target_faces, bool preserve_boundary = true) {
  // take into account, that the boundary faces cannot be refined
  if (preserve_boundary) {
    VCG::VcgMesh::VertexIterator vi;
    int bv = 0;
    for (vi = this->vert.begin(); vi != this->vert.end(); ++vi) {

      if (vi->IsD()) continue;
      if (vi->IsB()) ++bv;
    }
    target_faces += bv;
  }
  if (target_faces > this->FN()) return;

//  BoundaryWeight=.5;
//  CosineThr=cos(M_PI/2);
//  FastPreserveBoundary=false;
//  NormalCheck=false;
//  NormalThrRad=M_PI/2;
//  OptimalPlacement=true;
//  PreserveBoundary = false;
//  PreserveTopology = false;
//  QuadricEpsilon =1e-15;
//  QualityCheck=true;
//  QualityQuadric=false;
//  QualityThr=.3;        // higher the value -> better the quality of the accepted triangles
//  QualityWeight=false;
//  QualityWeightFactor=100.0;
//  ScaleFactor=1.0;
//  ScaleIndependent=true;
//  UseArea=true;
//  UseVertexWeight=false;

  vcg::tri::TriEdgeCollapseQuadricParameter params;
//  params.BoundaryWeight = .5;
  params.CosineThr = cos(M_PI / 2);
  params.FastPreserveBoundary = false;
  params.NormalCheck = false;
  params.NormalThrRad = M_PI / 2;
  params.OptimalPlacement = true;
  params.PreserveBoundary = preserve_boundary;
  params.PreserveTopology = false;
  params.QuadricEpsilon = 1e-15;
  params.QualityCheck = true;
  params.QualityQuadric = true;
  params.QualityThr = 1.;        // 0.3 default, higher the value -> better the quality of the accepted triangles
  params.QualityWeight = false;
  params.QualityWeightFactor = 100.0;
  params.ScaleFactor = 1.0;
  params.ScaleIndependent = true;
  params.UseArea = true;
  params.UseVertexWeight = false;

  vcg::LocalOptimization<VCG::VcgMesh> collapse(*this, &params);
  collapse.Init<MyTriEdgeCollapse>();

  collapse.SetTargetSimplices(target_faces);
  collapse.SetTimeBudget(0.5f);
  while (collapse.DoOptimization() && this->FN() > target_faces) {};
  collapse.Finalize<MyTriEdgeCollapse>();

  vcg::tri::Allocator<VCG::VcgMesh>::CompactFaceVector(*this);
  vcg::tri::Allocator<VCG::VcgMesh>::CompactVertexVector(*this);
}
