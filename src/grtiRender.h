#ifndef LUCIDSTAGE_GRTIRENDER_H
#define LUCIDSTAGE_GRTIRENDER_H

#include <Magick++.h>
#include "grtiData.h"
#include "utils.h"

class GrtiRender {
 public:
  size_t m_width;
  size_t m_height;
  size_t m_xData;
  size_t m_yData;
  boost::multi_array<float, 3> m_image_OIIO;
  Magick::Image m_Image;
  Magick::Geometry m_Geometry;
  GrtiData *m_GrtiDataPtr;

  GrtiRender(size_t w, size_t h, GrtiData *grtiDataPtr);

  void drawPoint(double phi, double theta, std::vector<double> color = {1, 0, 0}, int size = 10);
  void saveImage(std::string fp, boost::multi_array<float, 3> image);
  void saveEnvMap(std::string directory, std::string baseName);

  /**
   * R,G and B channel need to have the same number of verts at the moment
   *
   * @param x Pixel index in X
   * @param y Pixel index in Y
   * @return
   */
  // TODO: make channels independent
  void renderDots(const int x, const int y);

  /*!
   * This function renders a environment map for the pixel x and pixel y of the GRTI data set.
   *
   * @param x pixel coordinate in GRTI data set
   * @param y pixel coordinate in GRTI data set
   * @param pointer to lobes save location
   * @return
   */
  void renderEnvMap(const int xData,
                    const int yData,
                    const boost::filesystem::path outputPath,
                    const bool saveLobes = false);

  void renderAllMaps(const boost::filesystem::path outputPath,
                     const bool saveLobes = false);



  /*!
   * This function returns the barycentric weights for a point in space depending on the vtk mesh data.
   * If the point lays inside a polygon, the returnd weights will be zero for all points but the one making up the polygon.
   * If the point does not lay on any polygon, the list of weights bigger than zero will increase.
   *
   * @param myVtkMesh needs to have vtkPoints and vtkMesh set
   * @param phi is the azimuth
   * @param theta is the inclination
   * @param weights pointer to the list of weights, will be overwritten
   * @return
   */
  float getIntensity(VCG::VcgMesh *myVcgMesh, double phi, double theta, int channel);

};
#endif //LUCIDSTAGE_GRTIRENDER_H
